#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <string.h>

void init_odds(float *turn, float *river,int sz);
void init_odds_file(float *turn, float *river,int sz);
float get_odds(float pct);
float get_points(float delta);
float f_min(float a,float b);
float f_max(float a,float b);

int main()
{
	int input_ok = 0;
	int correct = 0;
	char* fName = "/Users/amacrae/Documents/Prog/C/outsNodds/highscores.txt";
	char read_buff[256];
	FILE *hiFile;
	int nbr_hands = 0;
	int max_hands = 10;
	
	float score = 0;
	float t_bonus = 0;
	int print_table = 0;
	
	int max_outs = 12;

	float* pct_turn = malloc(max_outs*sizeof(float));
	float* pct_river = malloc(max_outs*sizeof(float));
	int j;
	float tol = 0.1;
	
	char* tmp;
	time_t t;
	long time_s;
	srand((unsigned) time(&t));
	
	init_odds_file(pct_turn,pct_river,max_outs);
	
	if(print_table)
	{
		printf("\triver\t\t\tturn\nouts\tpercent\todds:1\t\tpercent\todds:1\n");
		for(j = 0;j<max_outs;j++)
		{
			printf("%d\t%0.1f\t%0.2f\t\t%0.1f\t%0.2f\n",j,pct_river[j],get_odds(pct_river[j]),pct_turn[j],get_odds(pct_turn[j]));
		}
	}
	float input = 0;
	int river = 0;
	float ans = 0;
	float delta = 0;
	while(nbr_hands < max_hands)
	{
		time(&t);
		time_s = (long)t;
		nbr_hands++;
		river = rand()%2;
		int outs = 1+rand()%(max_outs-1);
		if(river)
		{
			printf("\t--- HAND %d: You have %f points. --- \nYou have %d outs after the flop: what are the min pot odds:1 to play? :",nbr_hands,score,outs);
			while(!input_ok)
			{												
				fgets(read_buff, sizeof read_buff, stdin);
				input_ok = sscanf(read_buff, "%f",&input);
				if(!input_ok) printf("\nPlease enter a number: ");				
			}
			input_ok=0;
			time(&t);
			time_s=(long)t-time_s;
			ans = get_odds(pct_turn[outs]);		
			delta = fabs((input-ans)/ans);
			if(delta<tol) 
			{
				correct++;
				printf("\nCorrect!");
				if(delta==0) printf(" ...out by %f percent: dead on!",100*delta);
				else printf(" ... out by %f percent: close enough!",100*delta);
				printf("\n\t You said %f:1, correct answer is %f:1",input,ans);
				t_bonus = f_max(0.0,(float)(30-time_s)*5);
				score+=get_points(delta)+t_bonus;
				printf("\nElapsed Time: %lds. Time bonus: %0.1f points.\n",time_s,t_bonus);
				printf("\nAwarded %f points! Your score: %f.\n\n",get_points(delta),score);
			}
			else
			{
				printf("Incorrect ... out by %f percent.\n",100*delta);
				printf("\t You said %f:1, correct answer is %f:1\n",input,ans);
			}
		}
		else
		{
			printf("\t--- HAND %d: You have %f points. --- \nYou have %d outs after the turn: what are the min pot odds:1 to play? :",nbr_hands,score,outs);

			while(!input_ok)
			{								
				fgets(read_buff, sizeof read_buff, stdin);
				input_ok = sscanf(read_buff, "%f",&input);
				if(!input_ok) printf("\nPlease enter a number: ");				
			}
			input_ok=0;

			
			time(&t);
			time_s=(long)t-time_s;
			
			ans = get_odds(pct_river[outs]);		
			delta = fabs(input-ans)/ans;		
			if(delta<tol) 
			{
				correct++;
				printf("\nCorrect!");
				if(delta==0) printf(" ... out by %f percent: dead on!",100*delta);
				else printf(" ... out by %f percent: close enough!",100*delta);
				printf("\n\t You said %f:1, correct answer is %f:1\n",input,get_odds(pct_river[outs]));			
				t_bonus = f_max(0.0,(float)(30-time_s)*5);
				score+=get_points(delta)+t_bonus;
				printf("\nElapsed Time: %lds. Time bonus: %0.1f points.\n",time_s,t_bonus);
				printf("Awarded %f points! Your score: %f.\n\n",get_points(delta),score);				
			}
			else
			{
				printf("Incorrect ... out by %f percent.\n",100*delta);
				printf("\t You said %f:1, correct answer is %f:1\n",input,get_odds(pct_river[outs]));			
			}			
		}
	}
	printf("\n\n\t*************************\n");
	printf("\t*** Your Score: %0.1f ***\n",score);
	printf("\t******     %d/%d     *****\n",correct,nbr_hands);
	printf("\t*************************\n\n");

// Read hiscore file
	float hiScore = 0;
	hiFile = fopen(fName,"r");
	fscanf(hiFile,"%f",&hiScore);
	fclose(hiFile);
	if(score>hiScore)
	{
		printf("NEW HIGH SCORE!\nOld hi score:%f, your score:%f\n",hiScore,score);
		hiFile = fopen(fName,"w");
		fprintf(hiFile,"%f",score);
		fclose(hiFile);
	}

	return 0;
}

void init_odds(float *turn, float *river,int sz)
{ // Initialize the odds using the rule of two/four
	int j = 0;
	
	for(j=0;j<sz;j++)
	{
		river[j] = 2.0*j;
		turn[j] = 4.0*j;
	}
}

void init_odds_file(float *turn, float *river,int sz)
{ // Initialize the odds using a configuration file containing true odds
	int j = 0;
	FILE *infile;
	infile = fopen("/Users/amacrae/Documents/Prog/C/outsNodds/outsdata.csv","r");
	
	for(j=0;j<sz;j++)
	{
		fscanf(infile,"%f,%f",&river[j],&turn[j]);	
	}
	fclose(infile);
}

float get_odds(float pct)
{
	// convert percentage to minumum required pot odds
	return (100-pct)/pct;
}

float get_points(float delta)
{
	if(delta == 0) return 1;
	else return f_min(100,1/delta);
}

float f_min(float a,float b)
{
	if(a>b) return b;
	else return a;
}
float f_max(float a,float b)
{
	if(a>b) return a;
	else return b;
}